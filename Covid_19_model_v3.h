#include <vector>
using namespace std;


class Person {
public:
    int infected {0};
    int susceptible {0};
    int recovered {0};
    int dead {0};
    int quarantined {0};
    int social_distanced {0};
    float quarantine_prob {70};
	float social_distanced_prob {80};
    float unsocial_distance_prob {10};
	int days_infected {0};
	int incubation_period {0};
	int symptom_duration {0};
	int contagious_duration {0};
    float R_0 {2.5};
	float fatality_rate {2};
    float daily_fatality_rate {0};
    float daily_transmission_prob {0};

    Person(int infected, int susceptible, int days_infected);
    void covid_19_day(int current_day);
};

class Population {
public:
    int population_size {0};
    int initial_infected {0};
    int current_infected_count {initial_infected};
    vector<Person*> susceptible_population;
	vector<Person*> infected_population;
	vector<Person*> recovered_population;
	vector<Person*> dead_population;
	vector<Person*> social_distanced_population;
	vector<Person*> isolated_population;
	
	Population(int population_size, int initial_infected);
    void remove_recovered_and_dead();
	void transmission();
	void rotate_social_distanced();
	vector<Person*> get_total_living_population();
	vector<Person*> get_total_population();
};
