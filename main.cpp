#include "Covid_19_model_v3.cpp"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
using namespace std;

int main() {
    int current_day {0};
	int number_of_days = 350;
    int POPULATION_SIZE = 1'000'000;
	int INITIAL_INFECTED = 100;
	Population *population = new Population(POPULATION_SIZE, INITIAL_INFECTED);
	srand(time(NULL));
	string json_string = "{";
    while (current_day < number_of_days) {
		cout << "susceptible: " << population->susceptible_population.size() << endl;
		cout << "infected: " << population->infected_population.size() << endl;
		cout << "dead: " << population->dead_population.size() << endl;
		cout << "Percent dead: " << setprecision(3) << ((float) population->dead_population.size()) * 100 / POPULATION_SIZE << "%" << endl;
		vector<Person*> total_population = population->get_total_living_population();
		for (Person *person : total_population) {
            person->covid_19_day(current_day);
		}
		if (current_day > 60) {
            population->rotate_social_distanced();
		}
        population->remove_recovered_and_dead();
        population->transmission();
        current_day++;
		cout << "current_day: " << current_day << endl;
		cout << endl;
	}
    return 0;
}
