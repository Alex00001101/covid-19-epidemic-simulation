#include "Covid_19_model_v3.h"
#include <vector>
#include <iostream>
using namespace std;

Person::Person(int infected, int susceptible, int days_infected)
    : infected(infected),
      susceptible(susceptible),
	  days_infected(days_infected)
	{
        incubation_period = rand() % 13 + 2;
		symptom_duration = rand() % 4 + 10;
		contagious_duration = incubation_period + symptom_duration;
        daily_fatality_rate = (fatality_rate / symptom_duration) * 100;
        daily_transmission_prob = (R_0 / contagious_duration) * 100;
	}

void Person::covid_19_day(int current_day) {
    if (!dead) {
		if (infected) {
		   if (days_infected > incubation_period) {
			   if (current_day > 10) {
				   if ((rand() % 100) < quarantine_prob) {
					   quarantined = 1;
				   }
			   }
			   if ((rand() % 10000) <= daily_fatality_rate) {
				   dead = 1;
				   susceptible = 0;
				   infected = 0;
			   }
		   }
		   if (!dead) {
			   if (days_infected <= contagious_duration) {
				   days_infected++;
			   }
			   else if (days_infected > contagious_duration) {
				   infected = 0 ;
				   susceptible = 0;
				   recovered = 1;
			   }
		   }
		}
		else {
		   if (!social_distanced) {
			   if ((rand() % 100) < social_distanced_prob) {
				   social_distanced = 1;
			   }
		   }
		   else {
			   if ((rand() % 100) < unsocial_distance_prob) {
				   social_distanced = 0;
			   }
		   }
		}
	}
}

Population::Population(int population_size, int initial_infected) {
    population_size = population_size;
	initial_infected = initial_infected;

	for (int i = 0; i < (population_size - initial_infected); i++) {
		Person *tmp_person = new Person(0, 1, 0);
        susceptible_population.push_back(tmp_person);
	}
	for (int i = 0; i < initial_infected; i++) {
        int init_incubation_period = rand() % 13 + 2;
		int init_symptom_duration = rand() % 4 + 10;
		vector<int> inc_and_suscpt_periods = {init_incubation_period, init_incubation_period + init_symptom_duration};
		int zero_or_one = rand() % 2;
		int days_infected = rand() % inc_and_suscpt_periods[zero_or_one] + 2;
		Person *tmp_person = new Person(1, 0, days_infected);
		infected_population.push_back(tmp_person);
	}
}

void Population::remove_recovered_and_dead() {
    vector<int> remove_indices;
	vector<Person*> tmp_infected_population;
	int i = 0;
    for (Person *person : infected_population) {
        if (person->dead) {
            dead_population.push_back(person);
			remove_indices.push_back(i);
		}
		else if (person->recovered) {
            recovered_population.push_back(person);
			remove_indices.push_back(i);
		}
		else if (person->infected) {
            tmp_infected_population.push_back(person);
		}
		i++;
	}
	infected_population = tmp_infected_population;
}

void Population::transmission() {
    int transmission_count {0};
	for (Person *person : infected_population) {
        if (!person->quarantined) {
		    int random_int = rand() % 100;
            if (random_int < person->daily_transmission_prob) {
                transmission_count++;
			}
		}
	}
	if (susceptible_population.size() >= transmission_count) {
		for (int i = 0; i < transmission_count; i++) {
			Person *tmp_person = susceptible_population.back();
			susceptible_population.pop_back();
			tmp_person->infected = 1;
			tmp_person->susceptible = 0;
			infected_population.push_back(tmp_person);
		}
	}
}

void Population::rotate_social_distanced() {
    vector<Person*> tmp_susceptible_population;
    for (Person *person : susceptible_population) {
        if (person->social_distanced) {
            social_distanced_population.push_back(person);
		}
		else {
            tmp_susceptible_population.push_back(person);
		}
	}
    susceptible_population = tmp_susceptible_population;

	vector<Person*> tmp_social_distance_population;
	for (Person *person : social_distanced_population) {
        if (!person->social_distanced) {
            susceptible_population.push_back(person);
        }
		else {
		    tmp_social_distance_population.push_back(person);
		}
	}
	social_distanced_population = tmp_social_distance_population;
}

vector<Person*> Population::get_total_living_population() {
    vector<Person*> total_population;
	for (Person *person : infected_population) {
        total_population.push_back(person);
	}
	for (Person *person : susceptible_population) {
        total_population.push_back(person);
	}
	for (Person *person : recovered_population) {
        total_population.push_back(person);
	}
    for (Person *person : social_distanced_population) {
        total_population.push_back(person);
	}
	return total_population;
}

vector<Person*> Population::get_total_population() {
    vector<Person*> total_population;
	for (Person *person : infected_population) {
        total_population.push_back(person);
	}
	for (Person *person : susceptible_population) {
        total_population.push_back(person);
	}
	for (Person *person : recovered_population) {
        total_population.push_back(person);
	}
    for (Person *person : social_distanced_population) {
        total_population.push_back(person);
	}
	for (Person *person : dead_population) {
        total_population.push_back(person);
	}
	return total_population;
}
